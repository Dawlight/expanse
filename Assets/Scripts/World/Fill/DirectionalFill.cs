using System;
using System.Collections;
using UnityEngine;

namespace Unfold
{
  public class DirectionalFill
  {
    public static IEnumerator Fill(World world, Tile centerTile)
    {
      var time = 0.6f;
      var waitTime = new WaitForSecondsRealtime(time);

      Tile[] east = new Tile[world.Width / 2];
      Tile[] west = new Tile[world.Width / 2];
      Tile center = centerTile;
      Tile activeEast = centerTile;
      Tile activeWest = centerTile;

      for (int i = 0; i < world.Width / 2; i++)
      {
        yield return waitTime;
        var eastTile = world.GrowTile(activeEast.WorldPosition, Direction.East);
        var westTile = world.GrowTile(activeWest.WorldPosition, Direction.West);

        east[i] = eastTile;
        west[i] = westTile;

        activeEast = eastTile;
        activeWest = westTile;
      }

      yield return waitTime;

      Tile[] activeTilesNorthEast = new Tile[east.Length];
      Tile[] activeTilesSouthEast = new Tile[east.Length];
      Array.Copy(east, 0, activeTilesNorthEast, 0, east.Length);
      Array.Copy(east, 0, activeTilesSouthEast, 0, east.Length);

      Tile[] activeTilesNorthWest = new Tile[west.Length];
      Tile[] activeTilesSouthWest = new Tile[west.Length];
      Array.Copy(west, 0, activeTilesNorthWest, 0, west.Length);
      Array.Copy(west, 0, activeTilesSouthWest, 0, west.Length);

      Tile centerNorth = center;
      Tile centerSouth = center;

      for (int i = 0; i < world.Height / 2; i++)
      {
        yield return waitTime;

        for (int a = 0; a < east.Length; a++)
        {
          var activeTileNorthEast = activeTilesNorthEast[a];
          var activeTileSouthEast = activeTilesSouthEast[a];

          var northTile = world.GrowTile(activeTileNorthEast.WorldPosition, Direction.North);
          var southTile = world.GrowTile(activeTileSouthEast.WorldPosition, Direction.South);

          if (northTile != null)
            activeTilesNorthEast[a] = northTile;

          if (southTile != null)
            activeTilesSouthEast[a] = southTile;
        }

        for (int a = 0; a < west.Length; a++)
        {
          var activeTileNorthWest = activeTilesNorthWest[a];
          var activeTileSouthWest = activeTilesSouthWest[a];

          var northTile = world.GrowTile(activeTileNorthWest.WorldPosition, Direction.North);
          var southTile = world.GrowTile(activeTileSouthWest.WorldPosition, Direction.South);

          if (northTile != null)
            activeTilesNorthWest[a] = northTile;

          if (southTile != null)
            activeTilesSouthWest[a] = southTile;
        }

        if (world.Width % 2 > 0)
        {
          centerNorth = world.GrowTile(centerNorth.WorldPosition, Direction.North);
          centerSouth = world.GrowTile(centerSouth.WorldPosition, Direction.South);
        }
      }
    }
  }
}