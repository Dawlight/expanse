using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unfold
{
    public class StackFill
    {
        public static IEnumerator Fill(World world, Tile centerTile)
        {
            var time = world.FillDelay;
            var wait = new WaitForSecondsRealtime(time);

            List<Tile> unvisited = new List<Tile>();

            unvisited.Add(centerTile);

            while (unvisited.Count > 0)
            {
                var activeTile = unvisited[0];
                unvisited.RemoveAt(0);

                //Debug.Log($"[StackFille] Expanding {activeTile.WorldPosition.x}, {activeTile.WorldPosition.y}");

                //Debug.Log($"[StackFill] Trying North");
                var north = world.GrowTile(activeTile.WorldPosition, Direction.North);
                if (north != null) yield return wait;

                //Debug.Log($"[StackFill] Trying South");
                var south = world.GrowTile(activeTile.WorldPosition, Direction.South);
                if (south != null) yield return wait;

                //Debug.Log($"[StackFill] Trying East");
                var east = world.GrowTile(activeTile.WorldPosition, Direction.East);
                if (east != null) yield return wait;

                //Debug.Log($"[StackFill] Trying West");
                var west = world.GrowTile(activeTile.WorldPosition, Direction.West);
                if (west != null) yield return wait;

                //Debug.Log($"[StackFill] Trying SouthEast");
                var southEast = world.GrowTile(activeTile.WorldPosition, Direction.SouthEast);
                if (southEast != null) yield return wait;

                //Debug.Log($"[StackFill] Trying NorthWest");
                var northWest = world.GrowTile(activeTile.WorldPosition, Direction.NorthWest);
                if (northWest != null) yield return wait;

                //Debug.Log($"[StackFill] Trying SouthWest");
                var southWest = world.GrowTile(activeTile.WorldPosition, Direction.SouthWest);
                if (southWest != null) yield return wait;

                //Debug.Log($"[StackFill] Trying NorthEast");
                var northEast = world.GrowTile(activeTile.WorldPosition, Direction.NorthEast);
                if (northEast != null) yield return wait;

                if (north != null)
                    unvisited.Add(north);

                if (east != null)
                    unvisited.Add(east);

                if (south != null)
                    unvisited.Add(south);

                if (west != null)
                    unvisited.Add(west);

                if (northEast != null)
                    unvisited.Add(northEast);

                if (southEast != null)
                    unvisited.Add(southEast);

                if (southWest != null)
                    unvisited.Add(southWest);

                if (northWest != null)
                    unvisited.Add(northWest);
            }
        }
    }
}