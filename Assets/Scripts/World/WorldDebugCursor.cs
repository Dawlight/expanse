﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Unfold
{
    public class WorldDebugCursor : MonoBehaviour
    {
        private World _world;
        private AxisMapper _axisMapper;

        void Start()
        {
            _world = World.Instance;
            _axisMapper = AxisMapper.Instance;
        }
        // Update is called once per frame

        private void MoveCursor(Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    {
                        _world.SelectedTile = _world.ActiveTiles[new IntVector2 { x = _world.SelectedTile.WorldPosition.x, y = Mathf.Clamp(_world.SelectedTile.WorldPosition.y + 1, _world.WorldBounds.Min.y, _world.WorldBounds.Max.y) }];
                        var cursorPosition = _world.SelectionIndicator.localPosition;
                        var newPosition = new Vector3(_world.SelectedTile.WorldPosition.x * WorldConfig.TileWidth, 5, _world.SelectedTile.WorldPosition.y * WorldConfig.TileHeight);
                        _world.SelectionIndicator.transform.DOLocalMove(newPosition, 0.25f).SetEase(Ease.InOutCirc);
                        break;
                    }
                case Direction.South:
                    {
                        _world.SelectedTile = _world.ActiveTiles[new IntVector2 { x = _world.SelectedTile.WorldPosition.x, y = Mathf.Clamp(_world.SelectedTile.WorldPosition.y - 1, _world.WorldBounds.Min.y, _world.WorldBounds.Max.y) }];
                        var cursorPosition = _world.SelectionIndicator.localPosition;
                        var newPosition = new Vector3(_world.SelectedTile.WorldPosition.x * WorldConfig.TileWidth, 5, _world.SelectedTile.WorldPosition.y * WorldConfig.TileHeight);
                        _world.SelectionIndicator.transform.DOLocalMove(newPosition, 0.25f).SetEase(Ease.InOutCirc);
                        break;
                    }
                case Direction.East:
                    {
                        _world.SelectedTile = _world.ActiveTiles[new IntVector2 { x = Mathf.Clamp(_world.SelectedTile.WorldPosition.x + 1, _world.WorldBounds.Min.x, _world.WorldBounds.Max.x), y = _world.SelectedTile.WorldPosition.y }];
                        var cursorPosition = _world.SelectionIndicator.localPosition;
                        var newPosition = new Vector3(_world.SelectedTile.WorldPosition.x * WorldConfig.TileWidth, 5, _world.SelectedTile.WorldPosition.y * WorldConfig.TileHeight);
                        _world.SelectionIndicator.transform.DOLocalMove(newPosition, 0.25f).SetEase(Ease.InOutCirc);
                        break;
                    }
                case Direction.West:
                    {
                        _world.SelectedTile = _world.ActiveTiles[new IntVector2 { x = Mathf.Clamp(_world.SelectedTile.WorldPosition.x - 1, _world.WorldBounds.Min.x, _world.WorldBounds.Max.x), y = _world.SelectedTile.WorldPosition.y }];
                        var cursorPosition = _world.SelectionIndicator.localPosition;
                        var newPosition = new Vector3(_world.SelectedTile.WorldPosition.x * WorldConfig.TileWidth, 5, _world.SelectedTile.WorldPosition.y * WorldConfig.TileHeight);
                        _world.SelectionIndicator.transform.DOLocalMove(newPosition, 0.25f).SetEase(Ease.InOutCirc);
                        break;
                    }
            }

        }

        void Update()
        {

            if (_axisMapper.GetUp(PlayerConfig.LeftStick))
            {
                MoveCursor(Direction.North);
            }

            if (_axisMapper.GetDown(PlayerConfig.LeftStick))
            {
                MoveCursor(Direction.South);
            }

            if (_axisMapper.GetRight(PlayerConfig.LeftStick))
            {
                MoveCursor(Direction.East);
            }

            if (_axisMapper.GetLeft(PlayerConfig.LeftStick))
            {
                MoveCursor(Direction.West);
            }

            if (_world.IsShifting == false || true)
            {

                if (Input.GetButtonDown("A"))
                {
                    _world.InsertLine(_world.SelectedTile.WorldPosition, Direction.South);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.yellow;
                }

                if (Input.GetButtonDown("B"))
                {
                    _world.InsertLine(_world.SelectedTile.WorldPosition, Direction.East);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.red;
                }

                if (Input.GetButtonDown("X"))
                {
                    _world.InsertLine(_world.SelectedTile.WorldPosition, Direction.West);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.blue;
                }

                if (Input.GetButtonDown("Y"))
                {
                    _world.InsertLine(_world.SelectedTile.WorldPosition, Direction.North);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.green;
                }



                if (_axisMapper.GetDown(PlayerConfig.DPad))
                {
                    MoveCursor(Direction.South);
                    _world.ExtendEdge(Direction.South);
                    _world.RetractEdge(Direction.North);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.yellow;
                }

                if (_axisMapper.GetRight(PlayerConfig.DPad))
                {
                    MoveCursor(Direction.East);
                    _world.ExtendEdge(Direction.East);
                    _world.RetractEdge(Direction.West);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.red;
                }

                if (_axisMapper.GetLeft(PlayerConfig.DPad))
                {
                    MoveCursor(Direction.West);
                    _world.ExtendEdge(Direction.West);
                    _world.RetractEdge(Direction.East);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.blue;
                }

                if (_axisMapper.GetUp(PlayerConfig.DPad))
                {
                    MoveCursor(Direction.North);
                    _world.ExtendEdge(Direction.North);
                    _world.RetractEdge(Direction.South);
                    var meshRendere = _world.SelectionIndicator.GetComponent<MeshRenderer>();
                    meshRendere.material.color = Color.green;
                }
            }
        }
    }
}