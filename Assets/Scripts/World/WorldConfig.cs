
namespace Unfold
{
  public static class WorldConfig
  {
    public static float TileWidth = 10;
    public static float TileHeight = 10;

    public static uint Width = 9;
    public static uint Height = 9;
  }
}