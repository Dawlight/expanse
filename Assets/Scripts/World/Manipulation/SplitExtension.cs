using System;

namespace Unfold
{
    public static class SplitExtension
    {
        public static void Split(this World world, IntVector2 position, Direction direction)
        {
            Tile[] tiles = new Tile[0];

            var offsetX = 0;
            var offsetY = 0;

            var bounds = world.WorldBounds; // Copy by value 

            switch (direction)
            {
                case Direction.North:
                    bounds.Min.y = position.y + 1;
                    tiles = world.GetTilesAt(bounds);
                    offsetY = 1;
                    break;
                case Direction.East:
                    bounds.Min.x = position.x + 1;
                    tiles = world.GetTilesAt(bounds);
                    offsetX = 1;
                    break;
                case Direction.South:
                    bounds.Max.y = position.y - 1;
                    tiles = world.GetTilesAt(bounds);
                    offsetY = -1;
                    break;
                case Direction.West:
                    bounds.Max.x = position.x - 1;
                    tiles = world.GetTilesAt(bounds);
                    offsetX = -1;
                    break;
                default: throw new ArgumentOutOfRangeException("Only the main cardinal directions are allowed for splitting, for now.");
            }

            for (int i = 0; i < tiles.Length; i++)
            {
                var tile = tiles[i];
                tile.WorldPosition.y += offsetY;
                tile.WorldPosition.x += offsetX;
                world.ActiveTiles[tile.WorldPosition] = tile;
            }

            world.MoveTiles(tiles, direction);
        }
    }
}