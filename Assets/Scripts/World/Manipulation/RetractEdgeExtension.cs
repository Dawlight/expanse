﻿using UnityEngine;

namespace Unfold
{
    public static class RetractEdgeExtension
    {
        public static void RetractEdge(this World world, Direction direction)
        {
            var edgeTiles = world.GetEdge(direction);
            Debug.Log("Hey!");
            for (int i = 0; i < edgeTiles.Length; i++)
            {
                var tile = edgeTiles[i];
                tile.Origami.Furl(direction, world.WorldMoveTime, true);
                world.ActiveTiles.Remove(tile.WorldPosition);
            }


            switch (direction)
            {
                case Direction.North:
                    world.WorldBounds.Max.y -= 1;
                    break;
                case Direction.East:
                    world.WorldBounds.Max.x -= 1;
                    break;
                case Direction.South:
                    world.WorldBounds.Min.y += 1;
                    break;
                case Direction.West:
                    world.WorldBounds.Min.x += 1;
                    break;
            }
        }
    }
}
