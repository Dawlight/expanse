using UnityEngine;

namespace Unfold
{
    public static class ExtendEdgeExtension
    {
        public static void ExtendEdge(this World world, Direction direction)
        {
            var edgeTiles = world.GetEdge(direction);

            Debug.Log("Hey!");
            for (int i = 0; i < edgeTiles.Length; i++)
            {
                var tile = edgeTiles[i];
                world.GrowTile(tile.WorldPosition, direction, false);
            }

            switch (direction)
            {
                case Direction.North:
                    world.WorldBounds.Max.y += 1;
                    break;
                case Direction.East:
                    world.WorldBounds.Max.x += 1;
                    break;
                case Direction.South:
                    world.WorldBounds.Min.y -= 1;
                    break;
                case Direction.West:
                    world.WorldBounds.Min.x -= 1;
                    break;
            }
        }
    }
}