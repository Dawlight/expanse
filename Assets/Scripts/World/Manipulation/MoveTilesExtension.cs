using DG.Tweening;
using UnityEngine;

namespace Unfold
{
  public static class MoveTilesExtension
  {
    public static void MoveTiles(this World world, Tile[] tiles, Direction direction)
    {
      Vector3 moveVector = Vector3.zero;

      switch (direction)
      {
        case Direction.North:
          moveVector = new Vector3(0, 0, 1 * WorldConfig.TileHeight); break;
        case Direction.East:
          moveVector = new Vector3(1 * WorldConfig.TileWidth, 0, 0); break;
        case Direction.South:
          moveVector = new Vector3(0, 0, -1 * WorldConfig.TileHeight); break;
        case Direction.West:
          moveVector = new Vector3(-1 * WorldConfig.TileWidth, 0, 0); break;
      }

      for (var i = 0; i < tiles.Length; i++)
      {
        var tile = tiles[i];
        tile.transform.SetParent(world.MoveObject.transform);
      }

      var startPosition = world.MoveObject.transform.position;
      var endPosition = startPosition + moveVector;

      world.MoveObject.transform
      .DOMove(endPosition, world.WorldMoveTime)
      .SetEase(world.WorldMoveCurve)
      .OnComplete(() =>
      {
        var movePosition = world.MoveObject.transform.position;
        var roundedPosition = new Vector3(Mathf.Round(movePosition.x), Mathf.Round(movePosition.y), Mathf.Round(movePosition.z));
        world.MoveObject.transform.position = roundedPosition;

        for (var i = 0; i < tiles.Length; i++)
        {
          var tile = tiles[i];
          tile.transform.SetParent(world.transform);
        }
      });
    }
  }
}