using System;
using UnityEngine;

namespace Unfold
{
    public static class InsertLineExtension
    {
        public static void InsertLine(this World world, IntVector2 position, Direction direction)
        {
            Debug.Log($"[InsertLineExtension] Trying to insert line at {position.x}, {position.y}");

            if (world.IsShifting) return;

            world.IsShifting = true;

            world.WaitForWorldSplit();

            IntVector2 offset = new IntVector2 { };
            Direction verticality = Direction.None;

            var bounds = world.WorldBounds; // Copy by value


            switch (direction)
            {
                case Direction.North:
                    if (position.y >= bounds.Max.y) return;
                    offset.y = 1;
                    verticality = Direction.Horizontal;
                    break;
                case Direction.East:
                    if (position.x >= bounds.Max.x) return;
                    offset.x = 1;
                    verticality = Direction.Vertical;
                    break;
                case Direction.South:
                    if (position.y <= bounds.Min.y) return;
                    offset.y = -1;
                    verticality = Direction.Horizontal;
                    break;
                case Direction.West:
                    if (position.x <= bounds.Min.x) return;
                    offset.x = -1;
                    verticality = Direction.Vertical;
                    break;
            }

            Tile[] tiles = world.GetEdge(direction);
            var tileCopies = new Tile[tiles.Length];

            world.Split(position, direction);


            for (var i = 0; i < tiles.Length; i++)
            {
                var tile = tiles[i];

                var tileCopy = UnityEngine.Object.Instantiate(tile, world.transform);
                tileCopies[i] = tileCopy;

                if (verticality == Direction.Vertical)
                {
                    tileCopy.WorldPosition.x = position.x + offset.x;
                    tileCopy.WorldPosition.y = tile.WorldPosition.y;
                }
                else if (verticality == Direction.Horizontal)
                {
                    tileCopy.WorldPosition.x = tile.WorldPosition.x;
                    tileCopy.WorldPosition.y = position.y + offset.y;
                }

                world.ActiveTiles[tileCopy.WorldPosition] = tileCopy;
                world.ActiveTiles.Remove(tile.WorldPosition); // Tile has been moved in Split and received coordinates outside of active range. Hence removal.

                world.RepositionTile(tileCopy, tileCopy.WorldPosition);

                tileCopy.Origami.Unfold(direction, world.WorldMoveTime);
                tile.Origami.Furl(direction, world.WorldMoveTime, true);
            }
        }
    }
}