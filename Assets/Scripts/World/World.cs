﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Unfold;
using UnityEngine;

namespace Unfold
{
    public class World : Singleton<World>
    {
        public delegate void OnWorldMoveStart();
        public OnWorldMoveStart onWorldMoveStart;

        public delegate void OnWorlMoveComplete();
        public OnWorlMoveComplete onWorlMoveComplete;



        public bool IsShifting;

        public AnimationCurve TileFurlCurve;
        public AnimationCurve WorldMoveCurve;

        public Transform SelectionIndicator;

        public float WorldMoveTime;
        public Transform MoveObject;

        public Area StartingArea;
        public Tile StartingTile;

        public int Width;
        public int Height;

        public IntBounds WorldBounds;

        public Dictionary<IntVector2, Tile> ActiveTiles = new Dictionary<IntVector2, Tile>();

        public Player Player;
        public Camera Camera;
        private Tile _centerTile;
        public Tile SelectedTile;

        public float FillDelay;

        void Start()
        {
            onWorldMoveStart += WorldMoveStart;
            onWorlMoveComplete += WorldMoveComplete;

            RenderSettings.fog = false;
            if (Application.IsPlaying(gameObject))
            {
                StartingArea = StartingTile.Area;

                var startingTile = Instantiate(StartingTile, transform);
                startingTile.transform.position = transform.position;
                _centerTile = startingTile;

                SelectedTile = _centerTile;
                SelectionIndicator.localPosition = new Vector3(SelectedTile.WorldPosition.x * WorldConfig.TileWidth, 5, SelectedTile.WorldPosition.y * WorldConfig.TileHeight);

                var distance = transform.position - Player.transform.position;
                Player.transform.position += distance;
                Camera.transform.position += distance;

                WorldBounds = startingTile.WorldPosition.MaxBounds(Width, Height);
                ActiveTiles.Add(_centerTile.WorldPosition, _centerTile);

                StartCoroutine(StackFill.Fill(this, _centerTile));
            }
        }


        public void RepositionTile(Tile tile, IntVector2 position)
        {
            tile.transform.localPosition = this.GetWorldPosition(position);
        }

        public Tile GetTileAt(IntVector2 position)
        {
            Tile tile = null;
            ActiveTiles.TryGetValue(position, out tile);
            return tile;
        }

        public Tile[] GetTilesAt(IntBounds bounds)
        {
            var areaWidth = bounds.Width();
            var areaHeight = bounds.Height();
            var size = areaWidth * areaHeight;
            var tiles = new Tile[size];
            var coordinate = new IntVector2 { x = 0, y = 0 };

            for (int y = bounds.Min.y, i = 0; y <= bounds.Max.y; y++, i++)
            {
                for (int x = bounds.Min.x, j = 0; x <= bounds.Max.x; x++, j++)
                {
                    coordinate.x = x;
                    coordinate.y = y;
                    Tile tile;

                    ActiveTiles.TryGetValue(coordinate, out tile);

                    tiles[i * areaWidth + j] = tile;
                }
            }

            return tiles;
        }

        public Tile GrowTile(IntVector2 position, Direction direction, bool stopAtWorldBounds = true)
        {
            var bounds = new IntVector2 { }.MaxBounds(Width, Height);

            var xmin = bounds.Min.x;
            var xmax = bounds.Max.x;
            var ymin = bounds.Min.y;
            var ymax = bounds.Max.y;

            Tile tileToGrowFrom;
            var originTileExists = ActiveTiles.TryGetValue(position, out tileToGrowFrom);

            if (originTileExists == false) return null;

            var growingPosition = new IntVector2 { x = position.x, y = position.y }.OffsetDirection(direction);

            if (stopAtWorldBounds && (growingPosition.x > xmax || growingPosition.x < xmin || growingPosition.y > ymax || growingPosition.y < ymin)) return null;

            Tile growingTile;
            var growingTileExists = ActiveTiles.TryGetValue(growingPosition, out growingTile);

            if (growingTileExists) return null;

            Tile tileToInstantiate = null;

            switch (direction)
            {
                case Direction.North: tileToInstantiate = tileToGrowFrom.NorthernTile; break;
                case Direction.East: tileToInstantiate = tileToGrowFrom.EasternTile; break;
                case Direction.South: tileToInstantiate = tileToGrowFrom.SouthernTile; break;
                case Direction.West: tileToInstantiate = tileToGrowFrom.WesternTile; break;
                case Direction.NorthEast: tileToInstantiate = tileToGrowFrom.NorthernTile.EasternTile; break;
                case Direction.SouthEast: tileToInstantiate = tileToGrowFrom.SouthernTile.EasternTile; break;
                case Direction.SouthWest: tileToInstantiate = tileToGrowFrom.SouthernTile.WesternTile; break;
                case Direction.NorthWest: tileToInstantiate = tileToGrowFrom.NorthernTile.WesternTile; break;
                default: return null;
            }

            var tileInstance = Instantiate(tileToInstantiate, transform);


            tileInstance.WorldPosition = growingPosition;
            tileInstance.Origami.Unfurl(direction, 0.4f);
            tileInstance.name = $"{growingPosition.x}, {growingPosition.y}";

            tileInstance.transform.localPosition = this.GetWorldPosition(tileInstance.WorldPosition);

            ActiveTiles.Add(growingPosition, tileInstance);

            return tileInstance;
        }

        public Tile[] GetEdge(Direction direction)
        {
            var bounds = WorldBounds; // Copy by value

            switch (direction)
            {
                case Direction.North:
                    bounds.Min.y = WorldBounds.Max.y;
                    return GetTilesAt(bounds);
                case Direction.East:
                    bounds.Min.x = WorldBounds.Max.x;
                    return GetTilesAt(bounds);
                case Direction.South:
                    bounds.Max.y = WorldBounds.Min.y;
                    return GetTilesAt(bounds);
                case Direction.West:
                    bounds.Max.x = WorldBounds.Min.x;
                    return GetTilesAt(bounds);
                default: return new Tile[0];
            }
        }

        private void FurlTiles(Tile[] tiles, Direction direction)
        {
            for (int i = 0; i < tiles.Length; i++)
            {
                var tile = tiles[i];
                tile.Origami.Furl(direction, WorldMoveTime, true);
            }
        }

        private void WorldMoveComplete()
        {
            IsShifting = false;
            WorldAudio.Instance.PlayEndSplit();
            Camera.transform.DOShakePosition(0.5f, 1, 20, 90, false, true);
        }

        private void WorldMoveStart()
        {
            IsShifting = false;
            WorldAudio.Instance.PlayBeginSplit();
            Camera.transform.DOShakePosition(0.25f, 0.5f, 20, 90, false, true);
        }

        public void WaitForWorldSplit()
        {
            StartCoroutine(DoWaitForWorldSplit());
        }

        private IEnumerator DoWaitForWorldSplit()
        {
           
            onWorldMoveStart?.Invoke();
            yield return new WaitForSeconds(WorldMoveTime);
            onWorlMoveComplete?.Invoke();
        }

        void OnDestroy()
        {
            onWorldMoveStart -= WorldMoveStart;
            onWorlMoveComplete -= WorldMoveComplete;
        }
    }
}