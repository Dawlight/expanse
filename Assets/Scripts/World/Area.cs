﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Unfold
{
  public class Area : MonoBehaviour
  {
    [Serializable]
    public struct TileArrays
    {
      public Tile[] All;

      public Tile[] NorthBorder;
      public Tile[] EastBorder;
      public Tile[] SouthBorder;
      public Tile[] WestBorder;
    }

    public uint Width;
    public uint Height;
    public IntVector2 WorldPosition;

    public TileArrays Tiles;

    public World TargetWorld;

    public Area NorthArea;
    public Area EastArea;
    public Area SouthArea;
    public Area WestArea;

    public bool WrapNorth = true;
    public bool WrapEast = true;
    public bool WrapSouth = true;
    public bool WrapWest = true;

    void Update()
    {
      if (Application.IsPlaying(gameObject)) return;

    }

    public Tile GetTile(int x, int y)
    {
      if (y > Height && WrapNorth == false && NorthArea != null)
      {
        return NorthArea.GetTile(x, y);
      }

      if (x > Width && WrapEast == false && EastArea != null)
      {
        return EastArea.GetTile(x, y);
      }

      if (y < 0 && WrapSouth == false && SouthArea != null)
      {
        return SouthArea.GetTile(x, y);
      }

      if (x < 0 && WrapWest == false && WestArea)
      {
        return WestArea.GetTile(x, y);
      }

      int wrappedX = (int)(((x % Width) + Width) % Width);
      int wrappedY = (int)(((y % Height) + Height) % Height);

      return Tiles.All[wrappedY * Width + wrappedX];
    }

    public Tile GetTile(IntVector2 originAreaPosition)
    {
      return GetTile(originAreaPosition.x, originAreaPosition.y);
    }

    public Tile NorthernChunk(uint x)
    {
      x = x % Width;
      uint y = Height - 1;
      return Tiles.All[y * Width + x];
    }

    public Tile EasternChunk(uint y)
    {
      uint x = Width - 1;
      y = y % Height;
      return Tiles.All[y * Width + x];
    }


    public Tile SouthernChunk(uint x)
    {
      x = x % Width;
      uint y = 0;
      return Tiles.All[y * Width + x];
    }

    public Tile WesternChunk(uint y)
    {
      uint x = 0;
      y = y % Height;
      return Tiles.All[y * Width + x];
    }

    public float GetTotalWidth()
    {
      return Width * WorldConfig.TileWidth;
    }

    public float GetTotalHeight()
    {
      return Height * WorldConfig.TileHeight;
    }
  }
}