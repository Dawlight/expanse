using UnityEngine;
using static Unfold.OrigamiTile;

namespace Unfold
{
  public static class UnfurlTransform
  {
    public static OrigamiTransform Create(Direction direction)
    {
      var furl = new OrigamiTransform { };

      furl.PrimaryRotationAxis = Vector3.zero;
      furl.PrimaryTranslation = Vector3.zero;
      furl.SecondaryRotationAxis = Vector3.zero;
      furl.SecondaryTranslation = Vector3.zero;

      furl.PrimaryQuadrantA = 0;
      furl.PrimaryQuadrantB = 0;
      furl.SecondaryQuadrantA = 0;
      furl.SecondaryQuadrantB = 0;

      switch (direction)
      {
        case Direction.North:
        default:
          {
            furl.PrimaryQuadrantA = (int)Quadrant.NorthEast;
            furl.PrimaryQuadrantB = (int)Quadrant.NorthWest;

            furl.PrimaryRotationAxis = new Vector3(1, 0, 0);
            furl.PrimaryTranslation = new Vector3(0, -1, -1);

            furl.HandleRotationAxis = new Vector3(1, 0, 0);
            furl.HandleTranslation = new Vector3(0, -1, -1);

            break;
          }
        case Direction.East:
        case Direction.NorthEast:
        case Direction.SouthEast:
          {
            furl.PrimaryQuadrantA = (int)Quadrant.NorthEast;
            furl.PrimaryQuadrantB = (int)Quadrant.SouthEast;

            furl.PrimaryRotationAxis = new Vector3(0, 0, -1);
            furl.PrimaryTranslation = new Vector3(-1, -1, 0);

            furl.HandleRotationAxis = new Vector3(0, 0, -1);
            furl.HandleTranslation = new Vector3(-1, -1, 0);
            break;
          }
        case Direction.South:
          {
            furl.PrimaryQuadrantA = (int)Quadrant.SouthEast;
            furl.PrimaryQuadrantB = (int)Quadrant.SouthWest;

            furl.PrimaryRotationAxis = new Vector3(-1, 0, 0);
            furl.PrimaryTranslation = new Vector3(0, -1, 1);

            furl.HandleRotationAxis = new Vector3(-1, 0, 0);
            furl.HandleTranslation = new Vector3(0, -1, 1);
            break;
          }
        case Direction.West:
        case Direction.NorthWest:
        case Direction.SouthWest:
          {
            furl.PrimaryQuadrantA = (int)Quadrant.NorthWest;
            furl.PrimaryQuadrantB = (int)Quadrant.SouthWest;

            furl.PrimaryRotationAxis = new Vector3(0, 0, 1);
            furl.PrimaryTranslation = new Vector3(1, -1, 0);

            furl.HandleRotationAxis = new Vector3(0, 0, 1);
            furl.HandleTranslation = new Vector3(1, -1, 0);
            break;
          }
      }

      return furl;
    }
  }
}