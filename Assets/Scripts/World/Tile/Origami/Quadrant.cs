namespace Unfold
{
  public enum Quadrant
  {
    NorthEast = 0,
    NorthWest = 1,
    SouthWest = 2,
    SouthEast = 3
  }
}