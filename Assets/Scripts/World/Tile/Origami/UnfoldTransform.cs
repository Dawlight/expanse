using UnityEngine;

namespace Unfold
{
  public static class UnfoldTransform
  {
    public static OrigamiTransform Create(Direction direction)
    {
      var unfold = new OrigamiTransform { };

      unfold.PrimaryRotationAxis = Vector3.zero;
      unfold.PrimaryTranslation = Vector3.zero;
      unfold.SecondaryRotationAxis = Vector3.zero;
      unfold.SecondaryTranslation = Vector3.zero;

      unfold.PrimaryQuadrantA = 0;
      unfold.PrimaryQuadrantB = 0;
      unfold.SecondaryQuadrantA = 0;
      unfold.SecondaryQuadrantB = 0;

      switch (direction)
      {
        case Direction.North:
          {
            unfold.PrimaryRotationAxis = new Vector3(-1, 0, 0);
            unfold.PrimaryTranslation = new Vector3(0, 0, -2);
            unfold.SecondaryRotationAxis = new Vector3(1, 0, 0);

            unfold.PrimaryQuadrantA = (int)Quadrant.NorthEast;
            unfold.PrimaryQuadrantB = (int)Quadrant.NorthWest;
            unfold.SecondaryQuadrantA = (int)Quadrant.SouthWest;
            unfold.SecondaryQuadrantB = (int)Quadrant.SouthEast;

            break;
          }
        case Direction.East:
          {
            unfold.PrimaryRotationAxis = new Vector3(0, 0, 1);
            unfold.PrimaryTranslation = new Vector3(-2, 0, 0);
            unfold.SecondaryRotationAxis = new Vector3(0, 0, -1);

            unfold.PrimaryQuadrantA = (int)Quadrant.NorthEast;
            unfold.PrimaryQuadrantB = (int)Quadrant.SouthEast;
            unfold.SecondaryQuadrantA = (int)Quadrant.NorthWest;
            unfold.SecondaryQuadrantB = (int)Quadrant.SouthWest;
            break;
          }
        case Direction.South:
          {
            unfold.PrimaryRotationAxis = new Vector3(1, 0, 0);
            unfold.PrimaryTranslation = new Vector3(0, 0, 2);
            unfold.SecondaryRotationAxis = new Vector3(-1, 0, 0);

            unfold.PrimaryQuadrantA = (int)Quadrant.SouthEast;
            unfold.PrimaryQuadrantB = (int)Quadrant.SouthWest;
            unfold.SecondaryQuadrantA = (int)Quadrant.NorthEast;
            unfold.SecondaryQuadrantB = (int)Quadrant.NorthWest;
            break;
          }
        case Direction.West:
          {
            unfold.PrimaryRotationAxis = new Vector3(0, 0, -1);
            unfold.PrimaryTranslation = new Vector3(2, 0, 0);
            unfold.SecondaryRotationAxis = new Vector3(0, 0, 1);

            unfold.PrimaryQuadrantA = (int)Quadrant.NorthWest;
            unfold.PrimaryQuadrantB = (int)Quadrant.SouthWest;
            unfold.SecondaryQuadrantA = (int)Quadrant.NorthEast;
            unfold.SecondaryQuadrantB = (int)Quadrant.SouthEast;
            break;
          }
      }

      return unfold;
    }
  }
}