using UnityEngine;

namespace Unfold
{
  public struct OrigamiTransform
  {
    public Vector3 PrimaryRotationAxis;
    public Vector3 PrimaryTranslation;

    public Vector3 SecondaryRotationAxis;
    public Vector3 SecondaryTranslation;

    public Vector3 HandleRotationAxis;
    public Vector3 HandleTranslation;

    public int PrimaryQuadrantA;
    public int PrimaryQuadrantB;
    public int SecondaryQuadrantA;
    public int SecondaryQuadrantB;
  }
}