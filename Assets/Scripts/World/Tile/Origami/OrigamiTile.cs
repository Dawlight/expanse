﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Unfold
{

    public class OrigamiTile : MonoBehaviour
    {
        public Transform NorthEastQuadrant;
        public Transform NorthWestQuadrant;
        public Transform SouthWestQuadrant;
        public Transform SouthEastQuadrant;

        public Transform QuadrantsHandle;

        private Transform[] _quadrants;

        private static Vector3[] _restingPosition = new Vector3[]
        {
        new Vector3( 1, 0,  1),
        new Vector3(-1, 0,  1),
        new Vector3(-1, 0, -1),
        new Vector3( 1, 0, -1)
        };

        private OrigamiTransform _transform;
        private float _animationValue;
        private AnimationCurve _transformCurve;

        public void Unfold(Direction direction, float time)
        {
            _transform = UnfoldTransform.Create(direction);
            _animationValue = 0f;
            _transformCurve = World.Instance.WorldMoveCurve;
            AnimateUnfold(_transform, _animationValue);

            DOTween.To(() => _animationValue, x => _animationValue = x, 1f, time).SetEase(_transformCurve).OnUpdate(() =>
            {
                AnimateUnfold(_transform, _animationValue);
            }).OnComplete(() =>
            {
                var particles = Instantiate(EffectsLibrary.Instance.TileUnfoldParticles, null);
                particles.transform.position = transform.position;
            });
        }

        public void Unfurl(Direction direction, float time)
        {
            _transform = UnfurlTransform.Create(direction);
            _animationValue = 0f;
            _transformCurve = World.Instance.WorldMoveCurve;
            AnimateUnfurl(_transform, _animationValue);

            DOTween.To(() => _animationValue, x => _animationValue = x, 1f, time).SetEase(_transformCurve).OnUpdate(() =>
            {
                AnimateUnfurl(_transform, _animationValue);
            });
        }

        public void Furl(Direction direction, float time, bool destroy = false)
        {
            _transform = UnfurlTransform.Create(direction);
            _animationValue = 1f;
            _transformCurve = World.Instance.WorldMoveCurve;

            DOTween.To(() => _animationValue, x => _animationValue = x, 0f, time).SetEase(_transformCurve)
            .OnUpdate(() =>
             {
                 AnimateUnfurl(_transform, _animationValue);
             })
             .OnComplete(() =>
             {
                 if (destroy) Destroy(transform.parent.gameObject);
             });
        }

        private void AnimateUnfold(OrigamiTransform unfold, float value)
        {
            AnimateQuadrantUnfold(_quadrants[unfold.PrimaryQuadrantA], unfold.PrimaryRotationAxis, _restingPosition[unfold.PrimaryQuadrantA], unfold.PrimaryTranslation, value);
            AnimateQuadrantUnfold(_quadrants[unfold.PrimaryQuadrantB], unfold.PrimaryRotationAxis, _restingPosition[unfold.PrimaryQuadrantB], unfold.PrimaryTranslation, value);

            AnimateQuadrantUnfold(_quadrants[unfold.SecondaryQuadrantA], unfold.SecondaryRotationAxis, _restingPosition[unfold.SecondaryQuadrantA], Vector3.zero, value);
            AnimateQuadrantUnfold(_quadrants[unfold.SecondaryQuadrantB], unfold.SecondaryRotationAxis, _restingPosition[unfold.SecondaryQuadrantB], Vector3.zero, value);
        }

        private void AnimateQuadrantUnfold(Transform quadrant, Vector3 rotationAxis, Vector3 restingPosition, Vector3 translation, float value)
        {
            var degrees = Mathf.Rad2Deg * Mathf.Acos(value);

            var rotation = Quaternion.Euler(rotationAxis * degrees);
            var position = restingPosition + translation * (1f - value);

            quadrant.localRotation = rotation;
            quadrant.localPosition = position;
        }
        private void AnimateUnfurl(OrigamiTransform furl, float value)
        {
            AnimateQuadrantUnfurl(_quadrants[furl.PrimaryQuadrantA], furl.PrimaryRotationAxis, _restingPosition[furl.PrimaryQuadrantA], furl.PrimaryTranslation, value);
            AnimateQuadrantUnfurl(_quadrants[furl.PrimaryQuadrantB], furl.PrimaryRotationAxis, _restingPosition[furl.PrimaryQuadrantB], furl.PrimaryTranslation, value);

            AnimateQuadrantUnfurl(QuadrantsHandle, furl.HandleRotationAxis, Vector3.zero, furl.HandleTranslation, value);
        }

        public void AnimateQuadrantUnfurl(Transform quadrant, Vector3 rotationAxis, Vector3 restingPosition, Vector3 translation, float value)
        {
            var angle = Mathf.Acos(value);
            var deg = angle * Mathf.Rad2Deg;

            var cosHorizontal = Mathf.Cos(angle);
            var sinVertical = Mathf.Sin(angle);

            translation.x *= (1f - cosHorizontal);
            translation.y *= sinVertical;
            translation.z *= (1f - cosHorizontal);

            quadrant.localRotation = Quaternion.Euler(rotationAxis * deg);
            quadrant.localPosition = restingPosition + translation;
        }

        void Awake()
        {
            _quadrants = new Transform[]
                {
            NorthEastQuadrant,
            NorthWestQuadrant,
            SouthWestQuadrant,
            SouthEastQuadrant
                };
        }
    }
}