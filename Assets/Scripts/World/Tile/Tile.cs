﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Unfold
{
  //[ExecuteAlways]
  public class Tile : MonoBehaviour
  {
    public bool IsStatic = false;


    public IntVector2 WorldPosition;

    public Area Area;
    public IntVector2 OriginAreaPosition;


    public bool IsInOriginalPosition = true;

    public ulong Id;

    public Tile NorthernTile;
    public Tile EasternTile;
    public Tile SouthernTile;
    public Tile WesternTile;

    private BoxCollider _trigger;
    private Vector3 _startingPosition;
    private Area _areaOfOrigin = null;

    public Direction UnfoldDirection;
    private OrigamiTile _origami;
    private BoxCollider _boxCollider;

    public OrigamiTile Origami { get { return _origami; } }



    void Awake()
    {
      _trigger = GetComponent<BoxCollider>();
      _startingPosition = transform.position;
      _origami = GetComponentInChildren<OrigamiTile>();
      _boxCollider = GetComponent<BoxCollider>();
    }

    public void Unload()
    {
      transform.SetParent(_areaOfOrigin.transform);
      transform.position = _startingPosition;
    }

    public void Move(Vector3 position)
    {
      transform.position = position;
    }

    public void AnimateToPosition(Vector3 end)
    {
      transform.position = end + new Vector3(0, -10, 0);
      _trigger.enabled = false;
      StartCoroutine(DoAnimateToPoistion(end));
    }

    private IEnumerator DoAnimateToPoistion(Vector3 end)
    {
      Vector3 currentVelocity = new Vector3();
      var timeElapsed = 0f;
      while (timeElapsed < 0.0f)
      {

        transform.position = Vector3.SmoothDamp(transform.position, end, ref currentVelocity, 0.0f, 1000f, Time.deltaTime);
        timeElapsed += Time.deltaTime;
        yield return 0;
      }

      _trigger.enabled = true;
      transform.position = end;
    }

    public void OnTriggerEnter(Collider collider)
    {
      //var player = collider.transform.GetComponent<Player>();

      //World.Instance.GrowTile(WorldPosition, Direction.North);
      //World.Instance.GrowTile(WorldPosition, Direction.East);
      //World.Instance.GrowTile(WorldPosition, Direction.South);
      //World.Instance.GrowTile(WorldPosition, Direction.West);
    }
  }
}