﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unfold
{
    public static class PlayerConfig
    {
        public static readonly int LeftStick = 0;
        public static readonly int RightStick = 1;
        public static readonly int DPad = 2;
    }
}
