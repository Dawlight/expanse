﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Unfold
{
    public class Player : MonoBehaviour
    {
        private World _world;

        private Vector3 _relocationPosition;
        private bool _relocateNextFrame;


        public int ChunkCount { get; private set; }

        public AxisMapper LeftStickAxis;

        public Vector3 LeftHandOriginalLocalPosition;
        public Vector3 RightHandOriginalLocalPosition;

        public Transform ModelTransform;
        public Transform LeftHand;
        public Transform RightHand;

        public Tile LatestEnteredChunk { get; private set; }
        private Rigidbody _rigidBody;

        // Setings
        [SerializeField] private float _maxSpeed = 0f;
        [SerializeField] private float _maxAcceleration = 0f;
        [SerializeField] private float _frictionFactor = 0f;

        // Position
        private Vector3 _localPlayerPosition;
        private IntVector2 _worldCoordinates;
        private Vector3 _worldPosition;

        // Velocity
        private Vector2 _velocity;
        private Vector2 _velocityDirection;
        private float _velocityMagnitude;

        // Rotation
        private Quaternion _targetRotation;
        private Quaternion _currentRotation;
        private Vector3 _targetLookDirection;
        private Direction _cardinalDirection;


        // Grabbing related
        private Hands _hands;

        private Vector3 _anchoredPosition;
        private bool _isAnchored;
        private float _anchorMaxDistance = 2f;
        private Tile _draggingTile;
        private Direction _grabDirection;
        private Vector3 _dragVector = Vector3.zero;

        // Tweening
        public AnimationCurve TurnRateCurve;
        public AnimationCurve AccelerationCurve;
        private bool _movementIsDisabled;

        void Start()
        {
            _world = World.Instance;

            _rigidBody = GetComponent<Rigidbody>();
            LeftStickAxis = AxisMapper.Instance;

            LeftHandOriginalLocalPosition = LeftHand.localPosition + ModelTransform.localPosition;
            RightHandOriginalLocalPosition = RightHand.localPosition + ModelTransform.localPosition;

            _hands = new Hands(this, LeftHand, RightHand);
            _hands.onHandsAnchored += OnHandsAnchored;
            _hands.onHandsReleased += OnHandsReleased;

            _world.onWorldMoveStart += OnWorldMoveStart;
            _world.onWorlMoveComplete += OnWorldMoveComplete;
        }



        void LateUpdate()
        {
            var deltaTime = Time.deltaTime;

            var position = transform.position;
            position += new Vector3(_velocity.x * deltaTime, 0, _velocity.y * deltaTime);

            if (_isAnchored)
            {
                var anchorToPosition = _anchoredPosition - position;
                var anchorDirection = anchorToPosition.normalized;
                var distance = anchorToPosition.magnitude;

                if (distance > _anchorMaxDistance)
                {
                    var distanceDelta = Mathf.Abs(_anchorMaxDistance - distance);
                    _dragVector -= anchorDirection * distanceDelta * .5f;
                    position += anchorDirection * distanceDelta;
                } 
            }

            if (_dragVector.magnitude > 3f) OnDragComplete();

            Debug.DrawLine(transform.position, transform.position + _dragVector);

            position.y = 0;

            if (_movementIsDisabled == false)
                transform.position = position;
        }


        private void OnHandsAnchored(Tile tile, Direction direction, Vector3 anchorPosition)
        {
            _draggingTile = tile;
            _anchoredPosition = anchorPosition;
            _grabDirection = direction;
            Debug.Log($"[Player] Anchored and dragging {tile.WorldPosition.x}, {tile.WorldPosition.y}");
            _isAnchored = true;
        }

        private void OnHandsReleased()
        {
            _anchoredPosition = Vector3.zero;
            _dragVector = Vector3.zero;
            _isAnchored = false;
            _draggingTile = null;
            _grabDirection = Direction.None;
        }

        private void OnDragComplete()
        {
            var direction = _dragVector.GetCardinalDirection();
            var oppositeDirection = (_dragVector * -1).GetCardinalDirection();
            var position = _draggingTile.WorldPosition;
            transform.SetParent(_draggingTile.transform);

            //var offsetDirection = position.OffsetDirection(oppositeDirection);
            _world.InsertLine(position.OffsetDirection(oppositeDirection), direction);
            _hands.Release();
        }

        private void OnWorldMoveComplete()
        {
            _movementIsDisabled = false;
        }

        private void OnWorldMoveStart()
        {
            _movementIsDisabled = true;
        }

        void Update()
        {
            var axis = LeftStickAxis.GetAxis(PlayerConfig.LeftStick);
            axis = axis * AccelerationCurve.Evaluate(axis.magnitude);
            var axisMagnitude = axis.magnitude;

            ResolveVelocity(axis, axisMagnitude);
            ResolveRotation();
            _cardinalDirection = transform.forward.GetCardinalDirection();

            _localPlayerPosition = transform.localPosition;
            _worldCoordinates = new IntVector2 { x = Mathf.RoundToInt(_localPlayerPosition.x + WorldConfig.TileWidth / 2f) / (int)WorldConfig.TileWidth, y = Mathf.RoundToInt(_localPlayerPosition.z + WorldConfig.TileHeight / 2f) / (int)WorldConfig.TileHeight };
            _worldPosition = new Vector3(_worldCoordinates.x * WorldConfig.TileWidth, 0, _worldCoordinates.y * WorldConfig.TileHeight);

            if (Input.GetButtonDown("A"))
            {
                _hands.TryGrab();
            }

            if (Input.GetButtonUp("A"))
            {
                _hands.Release();
                _dragVector = Vector3.zero;
            }
        }

        public void ResolveVelocity(Vector2 axis, float axisMagnitude)
        {
            var acceleration = axis * _maxAcceleration;

            var targetVelocity = axisMagnitude * _maxSpeed;

            _velocity += acceleration * Time.deltaTime;
            _velocityDirection = _velocity.normalized;
            _velocityMagnitude = _velocity.magnitude;


            if (targetVelocity < _velocityMagnitude)  // So as to not stop completely when you let go of the analog stick.
            {
                _velocity += Vector2.ClampMagnitude(-_velocityDirection * _frictionFactor * _maxAcceleration * Time.deltaTime, _velocityMagnitude);
                _velocity = Vector3.ClampMagnitude(_velocity, _maxSpeed);
            }
            else
            {
                _velocity = Vector3.ClampMagnitude(_velocity, targetVelocity);
            }

            if (_velocity.sqrMagnitude < Vector3.kEpsilonNormalSqrt)
            {
                _velocity = Vector3.zero;
            }
            else
            {
                _targetLookDirection = new Vector3(_velocityDirection.x, 0, _velocityDirection.y); // We only do this so that the dude does not reset to looking forward if velocity vector is zero.
            }
        }

        public void ResolveRotation()
        {
            var velocityFactor = _velocityMagnitude / _maxSpeed;

            var deltaAngle = Vector3.SignedAngle(transform.forward, _targetLookDirection, Vector3.up);
            var angleFactor = deltaAngle / 180f;
            var turnRateModifier = TurnRateCurve.Evaluate(Mathf.Abs(angleFactor));


            _targetRotation = Quaternion.LookRotation(_targetLookDirection, Vector3.up);
            _currentRotation = transform.localRotation;

            var dragFactor = _dragVector.magnitude / 3f;

            transform.localRotation = Quaternion.RotateTowards(_currentRotation, _targetRotation, 360f * turnRateModifier * Time.deltaTime);
            ModelTransform.localRotation = Quaternion.Euler(velocityFactor * 20f + dragFactor * 70f, 0, -45f * angleFactor * turnRateModifier);
        }
    }
}