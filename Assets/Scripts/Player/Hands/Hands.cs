﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;

namespace Unfold
{
    public class Hands
    {
        public delegate void OnHandsAnchored(Tile anchoredTile, Direction direction,Vector3 anchorPosition);
        public OnHandsAnchored onHandsAnchored;

        public delegate void OnHandsReleased();
        public OnHandsReleased onHandsReleased;


        private Player _player;
        private Transform _leftHand;
        private Transform _rightHand;

        public Vector3 AnchorPoint;
        public bool IsAnchored = false;

        private Tweener _leftGrabTween;
        private Tweener _rightGrabTween;
        private TweenerCore<Vector3, Vector3, VectorOptions> _anchorGrabTween;
        private Tweener _leftHandReleaseTween;
        private Tweener _rightHandReleaseTween;

        private float _grabTime;

        public Hands(Player player, Transform leftHand, Transform rightHand)
        {
            _player = player;
            _leftHand = leftHand;
            _rightHand = rightHand;
            _grabTime = 0.2f;
        }


        public void TryGrab()
        {
            var forwardDirection = _player.transform.forward;
            var playerPosition = _player.transform.position;
            var anchorPosition = playerPosition + forwardDirection;

            var tileCoordinate = World.Instance.GetTileCoordinate(anchorPosition);
            var tile = World.Instance.GetTileAt(tileCoordinate);

            SetGrabTransform(tile, anchorPosition);
        }


        private void SetGrabTransform(Tile tile, Vector3 position)
        {
            _leftHandReleaseTween?.Pause();
            _rightHandReleaseTween?.Pause();

            _leftHand.SetParent(null);
            _rightHand.SetParent(null);

            var right = _player.transform.right * 0.5f;
            var left = -right;

            var leftPosition = position + left;
            var rightPosition = position + right;

            _leftGrabTween = _leftHand.DOMove(leftPosition, _grabTime).SetEase(Ease.InOutCubic);
            _rightGrabTween = _rightHand.DOMove(rightPosition, _grabTime).SetEase(Ease.InOutCubic);

            var direction = _player.transform.forward.GetCardinalDirection();

            AnchorPoint = _player.transform.position;
            _anchorGrabTween = DOTween.To(() => AnchorPoint, x => AnchorPoint = x, position, _grabTime).OnComplete(() => onHandsAnchored?.Invoke(tile, direction, AnchorPoint));
            IsAnchored = true;
        }


        public void Release()
        {
            ReturnHands();
            onHandsReleased?.Invoke();
        }

        private void ReturnHands()
        {
            _leftGrabTween?.Complete();
            _rightGrabTween?.Complete();
            _anchorGrabTween?.Complete();

            AnchorPoint = Vector3.zero;
            IsAnchored = false;

            var playerTransform = _player.transform;
            _leftHand.SetParent(playerTransform);
            _rightHand.SetParent(playerTransform);

            _leftHandReleaseTween = _leftHand.DOLocalMove(_player.LeftHandOriginalLocalPosition, _grabTime).SetEase(Ease.InOutCubic);
            _rightHandReleaseTween = _rightHand.DOLocalMove(_player.RightHandOriginalLocalPosition, _grabTime).SetEase(Ease.InOutCubic);
        }
    }
}
