﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFlatFollow : MonoBehaviour
{
    public Transform FollowTarget;
    private Vector3 _currentVelocity;
    private Vector3 _targetOffset;

    [SerializeField, Range(0f, 2f)] private float _smoothTime;
    [SerializeField, Range(0, 100f)] private float _maxSpeed;
    private bool _relocateNextFrame;
    private Vector3 _relocationPosition;

    void Start()
    {
        _targetOffset = transform.position - FollowTarget.position;

    }

    void LateUpdate()
    {
        if (_relocateNextFrame)
        {
            Debug.Log($"[Player] Trying to relocate!");
            transform.position = _relocationPosition;
            _relocateNextFrame = false;
            return;
        }

        var currentPos = transform.position;
        var targetPos = FollowTarget.position + _targetOffset;
        transform.position = Vector3.SmoothDamp(currentPos, targetPos, ref _currentVelocity, _smoothTime, _maxSpeed);
    }

    public void Relocate(Vector3 position)
    {
        _relocationPosition = position;
        _relocateNextFrame = true;
    }
}
