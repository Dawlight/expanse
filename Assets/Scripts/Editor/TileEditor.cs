﻿using UnityEditor;
using UnityEngine;

namespace Unfold
{

  [CanEditMultipleObjects]
  [CustomEditor(typeof(Tile))]
  public class TileEditor : Editor
  {

    public enum Direction
    {
      North = 0,
      East = 1,
      South = 2,
      West = 3
    }


    private string[] CarDirNames = new string[] { "North", "East", "South", "West" };


    private int _holdingHandle = -1;

    public override void OnInspectorGUI()
    {
      DrawDefaultInspector();

      //float northAnim = 0f;
      //GUILayout.HorizontalSlider(0, 0f, 1f)


      if (targets.Length != 2) return;

      var chunk0 = targets[0] as Tile;
      var chunk1 = targets[1] as Tile;

      var chunkDirection0 = GetCardinalDirection(chunk0.transform.position, chunk1.transform.position);
      var chunkDirection1 = (chunkDirection0 + 2) % 4;

      if (GUILayout.Button($"{chunk0.name}:{CarDirNames[chunkDirection0]} - {chunk1.name}:{CarDirNames[chunkDirection1]}"))
      {
        switch (chunkDirection0)
        {
          case (int)Direction.North: chunk0.NorthernTile = chunk1; break;
          case (int)Direction.East: chunk0.EasternTile = chunk1; break;
          case (int)Direction.South: chunk0.SouthernTile = chunk1; break;
          case (int)Direction.West: chunk0.WesternTile = chunk1; break;
        }

        switch (chunkDirection1)
        {
          case (int)Direction.North: chunk1.NorthernTile = chunk0; break;
          case (int)Direction.East: chunk1.EasternTile = chunk0; break;
          case (int)Direction.South: chunk1.SouthernTile = chunk0; break;
          case (int)Direction.West: chunk1.WesternTile = chunk0; break;
        }
      }

      if (GUILayout.Button($"Wrapped"))
      {
        switch (chunkDirection0)
        {
          case (int)Direction.North: chunk0.SouthernTile = chunk1; break;
          case (int)Direction.East: chunk0.WesternTile = chunk1; break;
          case (int)Direction.South: chunk0.NorthernTile = chunk1; break;
          case (int)Direction.West: chunk0.EasternTile = chunk1; break;
        }

        switch (chunkDirection1)
        {
          case (int)Direction.North: chunk1.SouthernTile = chunk0; break;
          case (int)Direction.East: chunk1.WesternTile = chunk0; break;
          case (int)Direction.South: chunk1.NorthernTile = chunk0; break;
          case (int)Direction.West: chunk1.EasternTile = chunk0; break;
        }
      }

    }


    private uint GetCardinalDirection(Vector3 origin, Vector3 other)
    {

      var originToOther = other - origin;
      var direction = new Vector2(originToOther.x, originToOther.z);

      Debug.DrawLine(origin, other);

      var xabs = Mathf.Abs(direction.x);
      var yabs = Mathf.Abs(direction.y);

      if (xabs < yabs)
      {
        if (direction.y > 0)
          return 0;
        return 2;
      }

      if (direction.x > 0)
        return 1;
      return 3;

    }

    void OnSceneGUI()
    {
      var chunk = target as Tile;

      if (chunk == null) return;

      switch (Event.current.type)
      {
        case EventType.MouseDown:
          break;
        case EventType.MouseUp:
          break;
        case EventType.Layout:
          break;
        case EventType.DragPerform:
        case EventType.DragUpdated:
        case EventType.DragExited:
          break;
        case EventType.Repaint:
          OnRepaint(chunk);
          break;
        default: break;

      }

      GUIStyle style = new GUIStyle();

      style.normal.textColor = Color.yellow;
    }

    private void OnRepaint(Tile chunk)
    {

      var chunkWidth = WorldConfig.TileWidth / 2f;
      var chunkHeight = WorldConfig.TileHeight / 2f;

      Vector3[] OffsetPositions = {
           new Vector3(0                        , 0 , chunkHeight * (1f - 0.1f)),
           new Vector3(chunkWidth * (1f - 0.1f) , 0 , 0),
           new Vector3(0                        , 0 ,chunkHeight * (-1f + 0.1f)),
           new Vector3(chunkWidth * (-1f + 0.1f), 0 , 0)
           };

      var chunkPos = chunk.transform.position;
      var colorNorth = Color.red;
      var colorEast = Color.green;
      var colorSouth = Color.blue;
      var colorWest = Color.yellow;

      Handles.color = Color.white;
      var labelStyle = new GUIStyle();
      labelStyle.fontStyle = FontStyle.Bold;
      Handles.Label(chunkPos + new Vector3(0, 5, 0), $"{chunk.WorldPosition.x}, {chunk.WorldPosition.y}", labelStyle);
      //labelStyle.fontStyle = FontStyle.BoldAndItalic;
      //Handles.Label(chunkPos + new Vector3(0, 10, 0), $"{chunk.OriginAreaPosition.x}, {chunk.OriginAreaPosition.y}", labelStyle);

      for (var i = 0; i < 4; i++)
      {
        var pressed = Handles.Button(chunk.transform.position + OffsetPositions[i], Quaternion.identity, 0.5f, 0.75f, Handles.SphereHandleCap);
        _holdingHandle = pressed ? i : -1;
      }

      if (chunk.NorthernTile != null)
      {
        Handles.color = colorNorth;
        Handles.Button(chunk.NorthernTile.transform.position + OffsetPositions[2], Quaternion.identity, 0.5f, 0.75f, Handles.SphereHandleCap);


        var offsetChunkPos = chunkPos + OffsetPositions[0];
        var northChunkPos = chunk.NorthernTile.transform.position + OffsetPositions[2];

        var halfDistance = Vector3.Distance(offsetChunkPos, northChunkPos) / 4;

        var middlePoint = (offsetChunkPos + northChunkPos) / 2;
        var chunkTangent = ((middlePoint + offsetChunkPos) / 2) + new Vector3(0, halfDistance, 0);
        var northChunkTangent = ((middlePoint + northChunkPos) / 2) + new Vector3(0, halfDistance, 0);

        Handles.DrawBezier(
           offsetChunkPos,
           northChunkPos,
           chunkTangent,
           northChunkTangent,
           colorNorth,
           null,
           4f);
      }

      if (chunk.EasternTile != null)
      {
        Handles.color = colorEast;
        Handles.Button(chunk.EasternTile.transform.position + OffsetPositions[3], Quaternion.identity, 0.5f, 0.75f, Handles.SphereHandleCap);
        //Handles.DrawLine(chunk.transform.position + OffsetPositions[1], chunk.EasternChunk.transform.position + OffsetPositions[3]);

        var offsetChunkPos = chunkPos + OffsetPositions[1];
        var eastChunkPos = chunk.EasternTile.transform.position + OffsetPositions[3];
        var halfDistance = Vector3.Distance(offsetChunkPos, eastChunkPos) / 4;

        var middlePoint = (offsetChunkPos + eastChunkPos) / 2;
        var chunkTangent = ((middlePoint + offsetChunkPos) / 2) + new Vector3(0, halfDistance, 0);
        var eastChunkTangent = ((middlePoint + eastChunkPos) / 2) + new Vector3(0, halfDistance, 0);

        Handles.DrawBezier(
           offsetChunkPos,
           eastChunkPos,
           chunkTangent,
           eastChunkTangent,
           colorEast,
           null,
           4f);
      }


      if (chunk.SouthernTile != null)
      {
        Handles.color = colorSouth;
        Handles.Button(chunk.SouthernTile.transform.position + OffsetPositions[0], Quaternion.identity, 0.5f, 0.75f, Handles.SphereHandleCap);


        var offsetChunkPos = chunkPos + OffsetPositions[2];
        var southChunkPos = chunk.SouthernTile.transform.position + OffsetPositions[0];
        var halfDistance = Vector3.Distance(offsetChunkPos, southChunkPos) / 4;

        var middlePoint = (offsetChunkPos + southChunkPos) / 2;
        var chunkTangent = ((middlePoint + offsetChunkPos) / 2) + new Vector3(0, halfDistance, 0);
        var eastChunkTangent = ((middlePoint + southChunkPos) / 2) + new Vector3(0, halfDistance, 0);

        Handles.DrawBezier(
           offsetChunkPos,
           southChunkPos,
           chunkTangent,
           eastChunkTangent,
           colorSouth,
           null,
           4f);
      }


      if (chunk.WesternTile != null)
      {
        Handles.color = colorWest;
        Handles.Button(chunk.WesternTile.transform.position + OffsetPositions[1], Quaternion.identity, 0.5f, 0.75f, Handles.SphereHandleCap);
        //Handles.DrawLine(chunk.transform.position + OffsetPositions[3], chunk.WesternChunk.transform.position + OffsetPositions[1]);

        var offsetChunkPos = chunkPos + OffsetPositions[3];
        var westernChunkPos = chunk.WesternTile.transform.position + OffsetPositions[1];
        var halfDistance = Vector3.Distance(offsetChunkPos, westernChunkPos) / 4;

        var middlePoint = (offsetChunkPos + westernChunkPos) / 2;
        var chunkTangent = ((middlePoint + offsetChunkPos) / 2) + new Vector3(0, halfDistance, 0);
        var eastChunkTangent = ((middlePoint + westernChunkPos) / 2) + new Vector3(0, halfDistance, 0);

        Handles.DrawBezier(
           offsetChunkPos,
           westernChunkPos,
           chunkTangent,
           eastChunkTangent,
           colorWest,
           null,
           4f);
      }
    }
  }
}