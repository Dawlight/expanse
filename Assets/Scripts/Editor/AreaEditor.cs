﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unfold;
using UnityEditor;
using UnityEngine;

namespace Unfold
{
  [CanEditMultipleObjects]
  [CustomEditor(typeof(Area))]
  public class AreaEditor : Editor
  {
    private string[] CarDirNames = new string[] { "North", "East", "South", "West" };

    public override void OnInspectorGUI()
    {
      DrawDefaultInspector();

      var area = target as Area;

      if (area.TargetWorld == null) return;

      if (GUILayout.Button("Layout Chunks"))
      {
        LayoutTiles();
      }

      if (GUILayout.Button("Connect Chunks"))
      {
        ConnectChunks(true);
      }

      if (targets == null || targets.Length != 2) return;

      var area0 = targets[0] as Area;
      var area1 = targets[1] as Area;

      var areaDirection0 = GetCardinalDirection(area0, area1);
      var areaDirection1 = (areaDirection0 + 2) % 4;

      if (GUILayout.Button($"{area0.name}:{CarDirNames[areaDirection0]} - {area1.name}:{CarDirNames[areaDirection1]}"))
      {

      }
    }

    private void ConnectAreas(Area area0, Area area1)
    {
      var areaDirection0 = GetCardinalDirection(area0, area1);
      var areaDirection1 = (areaDirection0 + 2) % 4;
    }

    private uint GetCardinalDirection(Area originArea, Area otherArea)
    {
      var originPos = originArea.transform.position;
      var otherPos = otherArea.transform.position;

      var originToOther = otherPos - originPos;
      var direction = new Vector2(originToOther.x, originToOther.z);

      Debug.DrawLine(originPos, otherPos);

      var xabs = Mathf.Abs(direction.x);
      var yabs = Mathf.Abs(direction.y);

      if (xabs < yabs)
      {
        if (direction.y > 0)
          return 0;
        return 2;
      }

      if (direction.x > 0)
        return 1;
      return 3;

    }

    private void LayoutTiles()
    {
      Area area = target as Area;
      World world = area.TargetWorld;

      List<Tile> childTiles = new List<Tile>();

      // Get all child tiles
      foreach (Transform child in area.transform)
      {
        Tile childTile = child.GetComponent<Tile>();
        if (childTile != null) childTiles.Add(childTile);
      }

      if (childTiles.Count < area.Width * area.Height)
      {
        Debug.LogError($"[AreaDefinitionEditor] Not enough tiles ({childTiles.Count}) to layout area definition, {area.Width} by {area.Height}"); return;
      }

      area.Tiles.All = childTiles.ToArray();

      area.Tiles.NorthBorder = new Tile[area.Width];
      area.Tiles.EastBorder = new Tile[area.Height];
      area.Tiles.SouthBorder = new Tile[area.Width];
      area.Tiles.WestBorder = new Tile[area.Height];

      var lowerLeftCorner = new Vector3(-WorldConfig.TileWidth * area.Width / 2, 0, -WorldConfig.TileHeight * area.Height / 2);

      for (var y = 0; y < area.Height; y++)
      {
        for (var x = 0; x < area.Width; x++)
        {
          // Layout the chunks in area space
          var tile = area.Tiles.All[y * area.Width + x];
          tile.OriginAreaPosition = new IntVector2 { x = x, y = y };
          tile.Area = area;
          tile.name = $"{area.name} - Tile {x}, {y}";
          tile.transform.localPosition = lowerLeftCorner + new Vector3(x * WorldConfig.TileWidth + WorldConfig.TileWidth / 2, 0, y * WorldConfig.TileHeight + WorldConfig.TileHeight / 2);

          // Cache chunks pertaining to each cardinal direction
          if (y == area.Height - 1) area.Tiles.NorthBorder[x] = tile;
          if (x == area.Width - 1) area.Tiles.EastBorder[y] = tile;
          if (y == 0) area.Tiles.SouthBorder[x] = tile;
          if (x == 0) area.Tiles.WestBorder[y] = tile;
        }
      }
    }


    private void ConnectChunks(bool wrapEdges)
    {
      Area area = target as Area;

      if (area.Tiles.All == null || area.Tiles.All.Length != area.Width * area.Height)
      {
        Debug.LogWarning($"[AreaDefinitionEditor] Number of chunks does not correspond to the width and height of the area.");
        return;
      }

      for (uint i = 0; i < area.Tiles.All.Length; i++)
      {
        var chunk = area.Tiles.All[i];
        Tile[] neightboringChunks = GetNeighboringChunks(i);

        chunk.NorthernTile = neightboringChunks[0];
        chunk.EasternTile = neightboringChunks[1];
        chunk.SouthernTile = neightboringChunks[2];
        chunk.WesternTile = neightboringChunks[3];
      }

    }

    private Tile[] GetNeighboringChunks(uint i)
    {
      Area area = target as Area;

      var width = area.Width;
      var height = area.Height;
      var size = area.Width * area.Height;


      // Transform to coordinate space;
      var x = i % width;
      var y = i / width;

      // Calculate wrapping coordinates for neighbors to the north, east, south and west.
      var nx = x;
      var ny = (y + 1) % height;

      var ex = (x + 1) % width;
      var ey = y;

      var sx = x;
      var sy = (y + height - 1) % height;

      var wx = (x + width - 1) % width;
      var wy = y;


      // Convert back to index space
      var ni = ny * area.Width + nx;
      var ei = ey * area.Width + ex;
      var si = sy * area.Width + sx;
      var wi = wy * area.Width + wx;

      return new Tile[] {
         area.Tiles.All[ni],
         area.Tiles.All[ei],
         area.Tiles.All[si],
         area.Tiles.All[wi]
      };
    }


    void OnSceneGUI()
    {
      Area area = target as Area;
      Vector3 areaPos = area.transform.position;
      World world = area.TargetWorld;

      if (world == null)
      {
        Debug.LogWarning($"[AreaDefinitionEditor] Please select a world for this area to use.");
        return;
      }


      var width = area.Width * WorldConfig.TileWidth;
      var height = area.Height * WorldConfig.TileHeight;

      // Debug.Log($"{width}, {height}");

      Vector3[] corners = new Vector3[] {
            new Vector3(areaPos.x - (width / 2f), areaPos.y, areaPos.z - (height / 2f)),
            new Vector3(areaPos.x - (width / 2f), areaPos.y, areaPos.z + (height / 2f)),
            new Vector3(areaPos.x + (width / 2f), areaPos.y, areaPos.z + (height / 2f)),
            new Vector3(areaPos.x + (width / 2f), areaPos.y, areaPos.z - (height / 2f))
        };

      switch (Event.current.type)
      {
        case EventType.Repaint:
          Handles.DrawSolidRectangleWithOutline(corners, Color.clear, Color.black);
          break;
      }
    }
  }
}