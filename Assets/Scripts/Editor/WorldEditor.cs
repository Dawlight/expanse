using UnityEditor;
using UnityEngine;


namespace Unfold
{
    [CustomEditor(typeof(World))]
    public class WorldEditor : Editor
    {
        void OnSceneGUI()
        {
            var world = target as World;

            if (world == null) return;
            var worldPos = world.transform.position;
            var worldWidth = world.Width;
            var worldHeight = world.Height;


            //for (var y = 0; y < WorldConfig.Height; y++)
            //{
            //    for (var x = 0; x < WorldConfig.Width; x++)
            //    {
            //        //var chunk = world.Chunks[y * WorldConfig.Width + x];
            //        //if (chunk != null)
            //        //    Handles.Label(
            //        //        new Vector3(x * chunkWidth, 0, y * chunkHeight) - new Vector3(WorldConfig.Width / 2 * WorldConfig.ChunkWidth, 0, WorldConfig.Height / 2 * WorldConfig.ChunkHeight),
            //        //        $"{x}, {y} / {chunk.Position.x}, {chunk.Position.y}");
            //    }
            //}

            Handles.BeginGUI();

            if (GUILayout.Button("Insert row"))
            {
                if (Application.IsPlaying(world))
                {
                    //world.InsertLine(3);
                }
            }

            Handles.EndGUI();

            float tileWidth = WorldConfig.TileWidth;
            float tileHeight = WorldConfig.TileHeight;

            float halfTileWidth = tileWidth / 2f;
            float halfTileHeight = tileHeight / 2f;

            var bounds = new IntVector2 { }.MaxBounds(world.Width, world.Height);

            var worldBoundaries = new Vector3[]
            {
                new Vector3(worldPos.x + bounds.Min.x * tileWidth - halfTileWidth, worldPos.y, worldPos.z + bounds.Min.y * tileHeight - halfTileHeight),
                new Vector3(worldPos.x + bounds.Min.x * tileWidth - halfTileWidth, worldPos.y, worldPos.z + bounds.Max.y * tileHeight + halfTileHeight),
                new Vector3(worldPos.x + bounds.Max.x * tileWidth + halfTileWidth, worldPos.y, worldPos.z + bounds.Max.y * tileHeight + halfTileHeight),
                new Vector3(worldPos.x + bounds.Max.x * tileWidth + halfTileWidth, worldPos.y, worldPos.z + bounds.Min.y * tileHeight - halfTileWidth)
            };

            Handles.DrawSolidRectangleWithOutline(worldBoundaries, Color.clear, Color.black);
        }
    }
}