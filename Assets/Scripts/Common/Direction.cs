﻿namespace Unfold
{
  public enum Direction
  {
    None = -1,
    North = 0,
    East = 1,
    South = 2,
    West = 3,
    Horizontal = 4,
    Vertical = 5,
    FurlNorth = 6,
    FurlEast = 7,
    FurlSouth = 8,
    FurlWest = 9,
    NorthEast = 10,
    SouthEast = 11,
    SouthWest = 12,
    NorthWest = 13
  }
}