using System;
using UnityEngine;

namespace Unfold
{
    [Serializable]
    public struct IntVector2 : IComparable<IntVector2>
    {
        public int x;
        public int y;

        public int CompareTo(IntVector2 obj)
        {
            var absThis = Mathf.Abs(this.x) + Mathf.Abs(this.y);
            var absObj = Math.Abs(obj.x) + Mathf.Abs(obj.y);
            var abs = absThis.CompareTo(absObj);

            if (abs != 0)
                return abs;

            var y = this.y.CompareTo(obj.y);

            if (y == 0)
                return this.x.CompareTo(obj.x);
            return y;
        }

        public static IntVector2 operator +(IntVector2 vector1, IntVector2 vector2)
        {
            vector1.x += vector2.x;
            vector1.y += vector2.y;
            return vector1;
        }

        public static IntVector2 operator -(IntVector2 vector1, IntVector2 vector2)
        {
            vector1.x -= vector2.x;
            vector1.y -= vector2.y;
            return vector1;
        }

        public static IntVector2 operator *(IntVector2 vector1, IntVector2 vector2)
        {
            vector1.x *= vector2.x;
            vector1.y *= vector2.y;

            return vector1;
        }

    }
}