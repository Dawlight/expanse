﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unfold
{
    [Serializable]
    public struct IntBounds
    {
        public IntVector2 Min;
        public IntVector2 Max;
    }
}
