﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Unfold
{
    public static class WorldExtensions
    {

        public static Vector3 GetPosition(this World world, IntVector2 coordinate)
        {
            var worldCenter = world.transform.position;
            return new Vector3(coordinate.x * WorldConfig.TileWidth + worldCenter.x, worldCenter.y, coordinate.y * WorldConfig.TileHeight + worldCenter.y);
        }

        public static Vector3 GetWorldPosition(this World world, IntVector2 coordinate)
        {
            return new Vector3(coordinate.x * WorldConfig.TileWidth, 0, coordinate.y * WorldConfig.TileHeight);
        }

        public static IntVector2 GetTileCoordinate(this World world, Vector3 position)
        {
            var x = Mathf.RoundToInt((position.x - world.transform.position.x) / WorldConfig.TileWidth);
            var y = Mathf.RoundToInt((position.z - world.transform.position.z) / WorldConfig.TileHeight);

            return new IntVector2 { x = x, y = y };
        }
    }
}
