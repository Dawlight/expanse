﻿using UnityEngine;

namespace Unfold
{
    public static class IntVector2Extensions
    {
        public static int ManhattanDistance(this IntVector2 vector, IntVector2 otherVector)
        {
            return Mathf.Abs(vector.x - otherVector.x) + Mathf.Abs(vector.y - otherVector.y);
        }

        public static IntVector2 OffsetDirection(this IntVector2 vector, Direction direction)
        {
            if (direction == Direction.North || direction == Direction.NorthEast || direction == Direction.NorthWest)
                vector.y += 1;

            if (direction == Direction.South || direction == Direction.SouthEast || direction == Direction.SouthWest)
                vector.y -= 1;

            if (direction == Direction.East || direction == Direction.NorthEast || direction == Direction.SouthEast)
                vector.x += 1;

            if (direction == Direction.West || direction == Direction.NorthWest || direction == Direction.NorthEast)
                vector.x -= 1;

            return vector;
        }

        public static IntBounds MaxBounds(this IntVector2 position, int width, int height)
        {
            int xmin, xmax, ymin, ymax;

            xmax = width / 2;
            ymax = height / 2;

            if (width % 2 == 0)
                xmin = -(xmax - 1); // Bias toward the left if no center tile
            else
                xmin = -xmax;

            if (height % 2 == 0)
                ymin = -(ymax - 1); // Bias toward the bottom if no center tile
            else
                ymin = -ymax;

            xmin += position.x;
            xmax += position.x;

            ymin += position.y;
            ymax += position.y;

            return new IntBounds
            {
                Max = new IntVector2 { x = xmax, y = ymax },
                Min = new IntVector2 { x = xmin, y = ymin }
            };
        }
    }
}