﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unfold
{
    public static class DirectionExtensions
    {

        public static Direction Opposite(this Direction direction)
        {
            switch (direction)
            {
                case Direction.North:
                    return Direction.South;
                case Direction.East:
                    return Direction.West;
                case Direction.South:
                    return Direction.North;
                case Direction.West:
                    return Direction.East;
                case Direction.NorthEast:
                    return Direction.SouthWest;
                case Direction.SouthEast:
                    return Direction.NorthWest;
                case Direction.NorthWest:
                    return Direction.SouthEast;
                case Direction.SouthWest:
                    return Direction.NorthEast;
                case Direction.Horizontal:
                    return Direction.Vertical;
                case Direction.Vertical:
                    return Direction.Horizontal;
                default: return Direction.None;
            }
        }
    }
}
