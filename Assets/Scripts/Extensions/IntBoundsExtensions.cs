﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unfold
{
    public static class IntBoundsExtensions
    {
        public static int Width(this IntBounds bounds)
        {
            return Math.Abs(bounds.Min.x - bounds.Max.x) + 1;
        }

        public static int Height(this IntBounds bounds)
        {
            return Math.Abs(bounds.Min.y - bounds.Max.y) + 1;
        }

        public static bool IsInside(this IntBounds bounds, IntVector2 position)
        {
            return position.x >= bounds.Min.x && position.x <= bounds.Max.x && position.y >= bounds.Min.y && position.y <= bounds.Max.y;
        }
    }
}
