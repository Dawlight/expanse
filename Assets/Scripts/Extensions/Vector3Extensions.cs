﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Unfold
{
    public static class Vector3Extensions
    {

        public static Direction GetCardinalDirection(this Vector3 vector)
        {
            Direction direction = Direction.None;


            if (Mathf.Abs(vector.x) > Mathf.Abs(vector.z))
            {
                if (vector.x > 0)
                    direction = Direction.East;
                else
                    direction = Direction.West;
            }
            else
            {
                if (vector.z > 0)
                    direction = Direction.North;
                else
                    direction = Direction.South;
            }

            return direction;
        }
    }
}
