﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Unfold
{
    public class AxisMapper : Singleton<AxisMapper>
    {
        [SerializeField]
        private AxisConfig[] _axisConfigs = new AxisConfig[0];

        private AxisState[] _axisStates = new AxisState[0];


        private void Start()
        {
            _axisStates = new AxisState[_axisConfigs.Length];
        }

        void Update()
        {

            for (int i = 0; i < _axisStates.Length; i++)
            {
                _axisStates[i].WasPreviousDeadZoned = _axisStates[i].IsCurrentDeadZoned;

                _axisStates[i].PreviousInput.x = _axisStates[i].CurrentInput.x;
                _axisStates[i].PreviousInput.y = _axisStates[i].CurrentInput.y;

                _axisStates[i].CurrentInput.x = Input.GetAxisRaw(_axisConfigs[i].Horizontal);
                _axisStates[i].CurrentInput.y = Input.GetAxisRaw(_axisConfigs[i].Vertical);

                if (_axisStates[i].CurrentInput.magnitude <= _axisConfigs[i].DeadZone)
                {
                    _axisStates[i].CurrentInput = Vector2.zero;
                    _axisStates[i].IsCurrentDeadZoned = true;
                }
                else
                {
                    _axisStates[i].CurrentInput = _axisStates[i].CurrentInput.normalized * ((_axisStates[i].CurrentInput.magnitude - _axisConfigs[i].DeadZone) / (1 - _axisConfigs[i].DeadZone));
                    _axisStates[i].IsCurrentDeadZoned = false;
                }

                //Debug.DrawLine(Vector3.zero, new Vector3(_axisStates[i].CurrentInput.x * 10f, 0, _axisStates[i].CurrentInput.y * 10f));
            }
        }

        public Vector2 GetAxis(int axisId)
        {
            return _axisStates[axisId].CurrentInput;
        }

        public bool GetLeft(int axisId)
        {
            return _axisStates[axisId].WasPreviousDeadZoned && _axisStates[axisId].CurrentInput.x < 0;
        }

        public bool GetRight(int axisId)
        {
            return _axisStates[axisId].WasPreviousDeadZoned && _axisStates[axisId].CurrentInput.x > 0;
        }

        public bool GetUp(int axisId)
        {
            return _axisStates[axisId].WasPreviousDeadZoned && _axisStates[axisId].CurrentInput.y > 0;
        }

        public bool GetDown(int axisId)
        {
            return _axisStates[axisId].WasPreviousDeadZoned && _axisStates[axisId].CurrentInput.y < 0;
        }

        private struct AxisState
        {
            public Vector2 CurrentInput;
            public Vector2 PreviousInput;

            public bool IsCurrentDeadZoned;
            public bool WasPreviousDeadZoned;
        }

        [Serializable]
        private struct AxisConfig
        {
            public string Horizontal;
            public string Vertical;
            public float DeadZone;
        }
    }
}