﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class ArmConnector : MonoBehaviour
{

    public Transform Object1;
    public Transform Object2;
    private LineRenderer _lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        _lineRenderer = GetComponent<LineRenderer>();

    }

    // Update is called once per frame
    void Update()
    {
        _lineRenderer.SetPosition(0, Object1.position);
        _lineRenderer.SetPosition(1, Object2.position);
    }
}
