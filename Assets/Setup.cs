﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


namespace Unfold
{
  public class Setup : Singleton<Setup>
  {
    void Start()
    {
      DOTween.Init(false, true, LogBehaviour.Verbose);
    }
  }
}