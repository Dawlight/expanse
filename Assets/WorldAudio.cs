﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Unfold
{
    public class WorldAudio : Singleton<WorldAudio>
    {

        public AudioClip BeginSplit;
        public AudioClip EndSplit;

        private AudioSource _audioSource;

        // Start is called before the first frame update
        void Start()
        {
            _audioSource = GetComponent<AudioSource>();
        }


        public void PlayBeginSplit()
        {
            _audioSource.clip = BeginSplit;
            _audioSource.Play();
        }

        public void PlayEndSplit()
        {
            _audioSource.clip = EndSplit;
            _audioSource.Play();
        }
    }
}