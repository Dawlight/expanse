﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{

    public float Delay;
    
    void Start()
    {
        StartCoroutine(DoKill());
    }

    private IEnumerator DoKill()
    {
        yield return new WaitForSeconds(Delay);
        Destroy(gameObject);
    }
}
